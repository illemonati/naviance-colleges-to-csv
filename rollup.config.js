import typescript from "rollup-plugin-typescript2";
import pkg from "./package.json";
import { nodeResolve } from "@rollup/plugin-node-resolve";
import babel from "rollup-plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import { terser } from "rollup-plugin-terser";

export default {
    input: "src/index.ts",
    output: {
        file: "dist/naviance-to-csv.js",
        format: "iife",
        name: "gamer",
        inlineDynamicImports: true,
    },
    plugins: [
        typescript({
            typescript: require("typescript"),
        }),
        babel({
            exclude: "node_modules/**",
        }),
        nodeResolve({
            browser: true,
        }),
        commonjs(),
        terser({
            output: {
                comments: "none",
            },
        }),
    ],
};
