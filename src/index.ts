import { downloadCsv } from "./download";
import {
    removeUnused,
    removeExtendedProfileAvailable,
    dataToCsv,
    dateToNumericalDateAndSort,
} from "./processing";

const main = async () => {
    const headers = [...getHeaders(), "Deadline Date"];
    const body = getBody(headers.indexOf("Deadline"));
    const all = [headers, ...body];
    const queue = [
        removeUnused,
        removeExtendedProfileAvailable,
        dateToNumericalDateAndSort,
    ];
    let final = all;
    for (const func of queue) {
        final = func(final);
    }
    const csvString = dataToCsv(final);
    downloadCsv(csvString);
};

const getHeaders = (): string[] => {
    return Array.from(document.querySelectorAll("th")).map(
        (th) => th.innerText
    );
};

const getBody = (dateColunmIndex: number): string[][] => {
    let res = Array.from(
        document.querySelector("tbody")!.querySelectorAll("tr")
    ).map((tr) => {
        return Array.from(tr.querySelectorAll("td")).map((td) => td.innerText);
    });
    res = dataConvertDate(res, dateColunmIndex);
    return res;
};

const dataConvertDate = (
    data: string[][],
    dateColunmIndex: number
): string[][] => {
    return data.map((row, i) => {
        const dateOriginal = row[dateColunmIndex];
        const dateOriginalSplit = dateOriginal.split("\n");
        if (dateOriginalSplit.length > 1) {
            const dateString = dateOriginalSplit[1];
            row.push(dateString);
        }
        return row;
    });
};

main().then();
