export const removeUnused = (data: string[][]) => {
    const headers = data[0];
    const unusedIndexes = headers
        .map((header, i) => (!header ? i : null))
        .filter((v) => v !== null);
    return data.map((row) => row.filter((_, i) => !unusedIndexes.includes(i)));
};

export const removeExtendedProfileAvailable = (data: string[][]) => {
    return data.map((r) =>
        r.map((c) => c.replace("extended profile available\n", ""))
    );
};

export const dataToCsv = (data: string[][]): string => {
    let result = "";
    for (const row of data) {
        for (const entry of row) {
            result += `"${entry}", `;
        }
        result += "\n";
    }
    return result;
};

export const dateToNumericalDateAndSort = (data: string[][]): string[][] => {
    const currentDate = new Date();
    const lastIndex = data[0].length - 1;
    const converted = data.map((row, i) => {
        if (i === 0) return row;
        const newRow = [...row] as any[];
        const dateOgString = row[lastIndex];
        if (dateOgString) {
            let date = new Date(`${dateOgString} ${currentDate.getFullYear()}`);
            if (date.getMonth() < 9) {
                date.setFullYear(date.getFullYear() + 1);
            }
            newRow[lastIndex] = date;
        }
        return newRow;
    });
    const sorted = converted.sort((a, b) => {
        if (a[lastIndex] && b[lastIndex]) {
            return a[lastIndex] - b[lastIndex];
        }
        if (!a[lastIndex]) {
            return 1;
        } else {
            return -1;
        }
    });

    return sorted.map((r, i) => {
        if (i === 0) return r;
        const date = r[lastIndex];
        r[lastIndex] = date instanceof Date ? getFormattedDate(date) : "";
        return r;
    });
};

const getFormattedDate = (date: Date): string => {
    let year = date.getFullYear();
    let month = (1 + date.getMonth()).toString().padStart(2, "0");
    let day = date.getDate().toString().padStart(2, "0");
    return month + "/" + day + "/" + year;
};
