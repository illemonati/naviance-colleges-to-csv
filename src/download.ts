export const downloadCsv = (csvString: string) => {
    const blob = new Blob([csvString], { type: "text/csv" });
    const a = document.createElement("a");
    a.href = URL.createObjectURL(blob);
    a.download = "schools.csv";
    document.body.appendChild(a);
    a.click();
    a.remove();
};
